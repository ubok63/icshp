﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cv_04
{
    public partial class Form1 : Form
    {
        Random random;
        Stats stats;



        public Form1()
        {
            InitializeComponent();
            random = new Random();
            stats = new Stats();
            stats.UpdatedStats += UPD_UpdatedStats;
            timer1.Tick += new EventHandler(Timer1_Tick);
        }

        private void UPD_UpdatedStats(object sender, EventArgs e)
        {
            correctLabel.Text = "Correct: " + stats.Correct.ToString();
            missedLabel.Text = "Missed: " + stats.Missed.ToString();
            accurancyLabel.Text = "Accuracy: " + stats.Accuracy.ToString();
        }


        private void Timer1_Tick(object sender, EventArgs e)
        {
            gameListBox.Items.Add((Keys)random.Next('A', 'Z'));
            if (gameListBox.Items.Count == 6)
            {
                timer1.Stop();
                MessageBox.Show("Game over!");
            }
        }

        private void LB_KeyDown(object sender, KeyEventArgs e)
        {
            if (gameListBox.Items.Contains(e.KeyCode))
            {
                gameListBox.Items.Remove(e.KeyCode);
                gameListBox.Refresh();
                stats.Update(true);
            }
            else {
                stats.Update(false);
            }

            if (timer1.Interval > 400)
            {
                timer1.Interval -= 60;
            }
            else if (timer1.Interval > 250)
            {
                timer1.Interval -= 15;
            }
            else if (timer1.Interval > 150)
            {
                timer1.Interval -= 8;
            }
            int value = 800 - timer1.Interval;
            if (value < 0 || value > 800) {
                timer1.Stop();
                MessageBox.Show("interval je mimo");
            }
            else
            {
                difficultyProgressBar.Value = value;
            }
        }

        private void StartOnClick(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void EndOnClick(object sender, EventArgs e)
        {
            timer1.Stop();
        }

        private void RestartOnClick(object sender, EventArgs e)
        {
            gameListBox.Items.Clear();
            timer1.Interval = 800;
            difficultyProgressBar.Value = 0;
            timer1.Start();
        }
    }
}
